# 

- [Behat and phpSpec tutorial](https://code.tutsplus.com/tutorials/a-bdd-workflow-with-behat-and-phpspec--cms-21601)
- [phpSpec tutorial](https://code.tutsplus.com/tutorials/getting-started-with-phpspec--cms-20919)
- [Behat doc](https://behat.org/en/latest/quick_start.html)

## Steps

1. install Behat and phpSpec:

    ```bash
    composer install
    ```

    ```bash
    vendor/bin/behat --init
    ```

1. create a behat `yourFeatureNameHere.feature` file in `features/` directory

1. create empty step definitions

    ```bash
    vendor/bin/behat --dry-run --append-snippets
    ```

1. implement steps logic in `FeatureContext` class

1. create phpSpec specification

    ```bash
    vendor/bin/phpspec desc "YourSpecClassNameHere"
    ```


#! /bin/bash

#SCRIPT_NAME=$(basename $0)
#SCRIPT_DIRECTORY=.
SCRIPT_NAME=$(basename $BASH_SOURCE)
SCRIPT_DIRECTORY=$(dirname $BASH_SOURCE)
EXIT_CODE_BYE=187

functionName() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} \e[33mparameterName\e[0m"

        return 1
    fi
}

##
## Composer
##

composer_install() {
    composer install
}

##
## Behat
##

behat() {
    vendor/bin/behat "$@"
}

init_behat() {
    behat --init
}

create_behat_empty_step_definitions() {
    behat --dry-run --append-snippets
}

##
## phpSpec
##

phpspec() {
    vendor/bin/phpspec "$@"
}

create_phpspec_spec() {
    phpspec desc "$@"
}

create_phpspec_spec() {
    phpspec desc "$@"
}

phpspec_run() {
    phpspec run --format=pretty
}

_handleExit() {
    if [ $? == $EXIT_CODE_BYE ]; then
        echo "Have a nice day!"
    fi
}

# Display the source code of this file
howItWorks() {
    cat $0
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]; then
    _listAvailableFunctions
    exit
fi

trap _handleExit exit err

"$@"

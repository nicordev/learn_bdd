<?php

class Config
{
    protected $settings;

    public function __construct()
    {
        $this->settings = [];
    }

    public static function load($path)
    {
        $config = new static();

        if (file_exists($path)) {
            $config->settings = include $path;
        }

        return $config;
    }

    public function get($option, $defaultValue = null)
    {
        if (!array_key_exists($option, $this->settings)) {
            return $defaultValue;
        }

        return $this->settings[$option];
    }

    public function set($option, $value)
    {
        $this->settings[$option] = $value;
    }
}

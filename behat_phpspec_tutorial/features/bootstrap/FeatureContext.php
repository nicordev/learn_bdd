<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    public const CONFIGURATION_FIXTURE_FILE = 'fixtures/config.php';

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    private function getConfigurationFixtures() {
        $configurationFixtures = include 'fixtures/config.php';

        if (!is_array($configurationFixtures)) {
            $configurationFixtures = [];
        }

        return $configurationFixtures;
    }

    private function updateConfigurationFixturesContent(array $configurationFixtures) {
        $content = "<?php\n\nreturn " . var_export($configurationFixtures, true) . ";\n";

        file_put_contents(self::CONFIGURATION_FIXTURE_FILE, $content);
    }

    /**
     * @Given there is a configuration file
     */
    public function thereIsAConfigurationFile()
    {
        if (!file_exists('fixtures/config.php')) {
            throw new RuntimeException("File 'fixtures/config.php' not found");
        }

        $configurationFixture = $this->getConfigurationFixtures();

        if (!is_array($configurationFixture)) {
            throw new RuntimeException("File 'fixtures/config.php' should contain an array");
        }
    }

    /**
     * @Given the option :option is configured to :value
     */
    public function theOptionIsConfiguredTo($option, $value)
    {
        $configurationFixtures = $this->getConfigurationFixtures();
        $configurationFixtures[$option] = $value;
        $this->updateConfigurationFixturesContent($configurationFixtures);
    }

    /**
     * @When I load the configuration file
     */
    public function iLoadTheConfigurationFile()
    {
        $this->config = Config::load('fixtures/config.php');
    }

    /**
     * @Then I should get :value as :option option
     */
    public function iShouldGetAsOption($value, $option)
    {
        $actualValue = $this->config->get($option);

        if ($value !== $actualValue) {
            throw new RuntimeException("Expected {$actualValue} to be '{$option}'.");
        }
    }

    /**
     * @Given the option :option is not yet configured
     */
    public function theOptionIsNotYetConfigured($option)
    {
        $configurationFixtures = $this->getConfigurationFixtures();
        unset($configurationFixtures[$option]);
        $this->updateConfigurationFixturesContent($configurationFixtures);
    }

    /**
     * @Then I should get default value :defaultValue as :option option
     */
    public function iShouldGetDefaultValueAsOption($defaultValue, $option)
    {
        $actualValue = $this->config->get($option, $defaultValue);

        if ($defaultValue !== $actualValue) {
            throw new RuntimeException("Expected {$option} to be '{$defaultValue}'.");
        }
    }

    /**
     * @When I set the :option configuration option to :value
     */
    public function iSetTheConfigurationOptionTo($option, $value)
    {
        $this->config->set($option, $value);
    }
}
